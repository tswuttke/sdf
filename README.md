# sdf

Ansible scripts to setup a Hadoop clustered environment with Spark additionally integrated. CDAP sandbox is also setup to develop Spark pipelines.

Notes: If using Vagrant, ensure that the VM's have the /etc/hosts file set with all of the nodes in the cluster (IP and hostname).

## Getting started

Install ansible (2.8+)

## Install Hadoop
- Update the hosts file to the relevant hostnames
- 
```
ssh-keygen -t rsa -f hadoop-key
ansible-playbook hadoop-cluster.yml -i hosts
```
Note: When generating the ssh key, do not set a password

## Install Spark

```
ansible-playbook spark-without-hadoop.yml -i hosts
```

## Install CDAP

```
ansible-playbook cdap.yml -i hosts
```

